<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# GitLab pipelines for Zephyr 

This repository contains definition of a GitLab pipeline for building Zephyr
apps. It can be used to simplify CI/CD for building specific Zephyr
applications for specific boards in public or private cloud.

Care is taken to make the process efficient both in time, money and space, with
the use of GitLab caches.

## Using the shared zephyr pipeline

To use the pipeline include the `zephyr.yml` file in your project by adding
this snippet to the `gitlab-ci.yml` file:

``` yaml
include:
  - project: 'zygoon/zephyr-ci-pipelines'
    file: '/zephyr.yml'
```

This defines two jobs, `.zephyr` and `zephyr-cache`, read on to find out more.

## Using the .zephyr job

The `.zephyr` installs all required dependencies, sets up zephyr workspace,
installs Zephyr SDK. The working directory to the root of the zephyr workspace.
You can use it to simplify building arbitrary projects and board combinations.

For example, this job builds the _blinky_ sample for the _nRF52840DK_ board.

``` yaml
blinky:
  extends: .zephyr
  variables:
    CI_ZEPHYR_BOARD: nrf52840dk_nrf52840
    CI_ZEPHYR_SDK_TOOLCHAIN: arm-zephyr-eabi
  script:
    - west build -p auto -b "$CI_ZEPHYR_BOARD" zephyr/samples/basic/blinky
```

There are a number of variables that allow to fine tune and optimize the
process. 

The variable `CI_ZEPHYR_MANIFEST_URL` controls the URL of the Zephyr manifest.
It defaults to the upstream zephyr repository but you can use it to redirect
the pipeline to your fork.

The variable `CI_ZEPHYR_MANIFEST_REV` controls the revision of the manifest to
prepare. It defaults to _v3.1.0_ and this version will move over time, as
subsequent releases are made. You can set it to `main` to test bleeding edge
versions.

The variable `CI_ZEPHYR_SDK_VERSION` controls the version of the SDK to use. It
defaults to _0.14.2_ and similarly to manifest revision, it will move over time
to stay in sync with the rest of the project.

The variable `CI_ZEPHYR_SDK_TOOLCHAIN` controls which SDK tool-chains to
install. It defaults to `all` which (possibly wastefully) installs all the
available tool-chains. As of SDK 0.14.2 you can set it to one of the following
values:

 - `aarch64-zephyr-elf`
 - `arc64-zephyr-elf`
 - `arc-zephyr-elf`
 - `arm-zephyr-eabi`
 - `mips-zephyr-elf`
 - `nios2-zephyr-elf`
 - `riscv64-zephyr-elf`
 - `sparc-zephyr-elf`
 - `x86_64-zephyr-elf`
 - `xtensa-espressif_esp32_zephyr-elf`
 - `xtensa-espressif_esp32s2_zephyr-elf`
 - `xtensa-intel_apl_adsp_zephyr-elf`
 - `xtensa-intel_bdw_adsp_zephyr-elf`
 - `xtensa-intel_byt_adsp_zephyr-elf`
 - `xtensa-intel_s1000_zephyr-elf`
 - `xtensa-nxp_imx_adsp_zephyr-elf`
 - `xtensa-nxp_imx8m_adsp_zephyr-elf`
 - `xtensa-sample_controller_zephyr-elf`

You should set it to the name of the tool-chain that corresponds to the board
you are using.

Lastly the variable `CI_ZEPHYR_WORKSPACE` controls the path, relative to
`$CI_PROJECT_DIR` where the workspace is created. It defaults to
`zephyrproject` and the intent of the variable is so that various other places
can refer to it rather than hard-code it. The default is good and probably
won't need changing.

The job contains three caches, one for the binary SDK, one for the workspace
and one for the Python pip cache. Those are managed automatically and should
not need tweaking. Please open issues if the cache is misbehaving.

Lastly the job has a default artifact selection logic. The job will pull a
number of binary files from that may be present in
`${CI_ZEPHYR_WORKSPACE}/build/zephyr`. This selection is somewhat arbitrary but
is meant to be useful. Please report issues if useful or essential files are
not included. 

## Using the zephyr-cache job

The `zephyr-cache` maintains shared cache in your project. It keeps the Zephyr
workspace, python dependencies and SDK available as cached elements. This
significantly speeds up the build process.

This job only runs in scheduled pipelines. You should add one to your project
and set the desired run frequency (e.g. daily or weekly).

Note that this cache job is only meaningful when either:

 - all runners use the same cache (shared, distributed cache)
 - there is only one runner using local cache
 - caches are shared between protected and non-protected branches
   https://docs.gitlab.com/ee/ci/caching/#use-the-same-cache-for-all-branches

Improvements are possible and contributions are welcome. If the _cache policy_
is made configurable the cache system can be used without the scheduled job on
a group of standalone runners without distributed cache.

## Using the shared j-link pipeline

To use the pipeline include the `j-link.yml` file in your project by adding
this snippet to the `gitlab-ci.yml` file:

``` yaml
include:
  - project: 'zygoon/zephyr-ci-pipelines'
    file: '/j-link.yml'
```

You also have to register a GitLab runner which has access to your J-Link
device, either the Ethernet-based J-Link Pro or any of the USB models. The
pipeline is tested with one of the J-Link OB (on-board) low-cost development
kits. The USB device is passed through to an LXD container using a custom LXD
executor developed separately at https://gitlab.com/zygoon/gitlab-lxd-executor

There are two configuration elements: an LXD profile to pass-through the
programmer and a registration of the custom executor.

An example LXD profile that passes through nRF52840 DK-based J-Link OB is:

``` yaml
config: {}
description: Pass-through nRF52840DK J-Link OB USB device
devices:
  jlink:
    productid: "1050"
    type: usb
    vendorid: "1366"
name: nrf52840dk
used_by: []
```

Register a new GitLab runner, with tags `lxd`, `j-link` and `nrf52840dk` and
edit the configuration file `/etc/gitlab-runner/config.toml` to configure the
custom executor. The important parts are augmented with a comment.

``` toml
[[runners]]
  name = "r-blade-lxd-jlink-nrf52840dk"
  url = "https://gitlab.com/"
  token = "REDACTED"
  executor = "custom"
  # Allow CI jobs to know the serial number of the J-Link to use.
  environment = ["CI_JLINK_SERIAL=1050292001"]
  # Serialize access to this executor so that the programmer is only connected
  # to one container at a time.
  limit = 1
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.custom]
    config_exec = "/usr/local/bin/gitlab-lxd-executor"
    # The Nordic development kit is physically connected to the LXD cluster
    # member called "blade". The argument "--node" can be removed when
    # LXD clustering is not used.
    config_args = ["config", "--profile", "nrf52840dk", "--node", "blade"]
    prepare_exec = "/usr/local/bin/gitlab-lxd-executor"
    prepare_args = ["prepare"]
    run_exec = "/usr/local/bin/gitlab-lxd-executor"
    run_args = ["run"]
    cleanup_exec = "/usr/local/bin/gitlab-lxd-executor"
    cleanup_args = ["cleanup"]
```

## Using the .j-link job

With the custom executor configured you should be able to run a sample job
which can flash arbitrary program.

``` yaml
flash-blinky:
  needs: [blinky]
  extends: [.j-link]
  tags: [lxd, j-link, nrf52840dk]
  script:
    - test -n "$CI_JLINK_SERIAL" || (
        echo "The variable CI_LINK_SERIAL must be provided by the GitLab runner" >&2
        exit 1
      )
    # The flashing script expects the file "zephyr.hex" in the current directory.
    - ln -s "${CI_ZEPHYR_WORKSPACE}/build/zephyr/zephyr.hex"
    - JLinkExe
      -NoGui 1
      -ExitOnError 1
      -USB "$CI_JLINK_SERIAL"
      -Device NRF52840_XXAA
      -If SWD
      -Speed 4000
      -CommandFile flash.j-link
    - fromdos j-link.log
  artifacts:
    paths:
      - j-link.log
```

This job uses the `flash.j-link` scrip with the following contents. You may
need to adjust this depending on the specific project you have. This script is
not a part of the shared pipeline that you can include, so please have a
customized copy in your repository:

```
// Log all interactions to j-link.log.
Log j-link.log

// This script resets, halts and programs the board using Segger J-Link.
// Command reference: https://wiki.segger.com/J-Link_Commander#Commands
// The specific probe and details about the connected device need to be
// provided as command line arguments to JLinkExe.
Connect
Reset
Halt
LoadFile zephyr.hex
Exit
```

If you want to perform other tests on the device, do it in the same job so that
the state of the programmed device is known. Separating this step to a distinct
job does not guarantee the state of the programmed device, as another job may
be scheduled that will wipe and replace the state.

There is one variable that allow to customize the process a little.

The variable `CI_JLINK_VERSION` pins the version of J-Link software to use. At
the moment it is set to 770 but this value will change over time, as new
releases are made. If you want to test against a fixed version of the software
define the variable to the version you want. Segger publishes updates at
https://www.segger.com/downloads/jlink/

## Contributions

Contributions are welcome. Share your experience if you are using this in your
project.

## License and REUSE

This project is licensed under the Apache 2.0 license, see the `LICENSE` file
for details. The project is compliant with https://reuse.software/ - making
it easy to ensure license and copyright compliance by automating software
bill-of-materials. In other words, it's a good citizen in the modern free
software stacks.
