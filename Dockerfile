# SPDX-FileCopyrightText: 2022 eclipse foundation
#
# SPDX-License-Identifier: EPL-2.0

ARG BASE_IMAGE=ubuntu
ARG BASE_IMAGE_VERSION=22.04
FROM ${BASE_IMAGE}:${BASE_IMAGE_VERSION}

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ARG ZEPHYR_SDK_VERSION=0.15.0
ARG ZEPHYR_WORKSPACE=/zephyrproject
ARG ZEPHYR_SDK_INSTALL_DIR=/opt
ARG ZEPHYR_SDK_TOOLCHAIN=all
ARG ZEPHYR_MANIFEST_URL=https://github.com/zephyrproject-rtos/zephyr
ARG ZEPHYR_MANIFEST_REV=v3.1.0
ENV DEBIAN_FRONTEND=noninteractive

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install -y --no-install-recommends eatmydata && \
    eatmydata apt-get install -y --no-install-recommends \
      git cmake ninja-build gperf ccache dfu-util device-tree-compiler wget \
      python3-dev python3-pip python3-setuptools python3-tk python3-wheel \
      xz-utils file make gcc gcc-multilib g++-multilib libsdl2-dev libmagic1 \
      \
    && eatmydata apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR "${ZEPHYR_WORKSPACE}"

# hadolint ignore=DL3013,SC2164,DL3003
RUN pip3 install --no-cache-dir -U west && \
  wget -q --show-progress --progress=bar:force:noscroll -O "/tmp/zephyr-sdk-${ZEPHYR_SDK_VERSION}_linux-$(uname -m).tar.gz" "https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v${ZEPHYR_SDK_VERSION}/zephyr-sdk-${ZEPHYR_SDK_VERSION}_linux-$(uname -m).tar.gz" && \
  tar xf "/tmp/zephyr-sdk-${ZEPHYR_SDK_VERSION}_linux-$(uname -m).tar.gz" -C "${ZEPHYR_SDK_INSTALL_DIR}" &&  \
  rm -f "/tmp/zephyr-sdk-${ZEPHYR_SDK_VERSION}_linux-$(uname -m).tar.gz" &&  \
  cd "${ZEPHYR_SDK_INSTALL_DIR}/zephyr-sdk-${ZEPHYR_SDK_VERSION}"; ./setup.sh -t "${ZEPHYR_SDK_TOOLCHAIN}" -c && \
  chmod -R 775 "${ZEPHYR_SDK_INSTALL_DIR}/zephyr-sdk-${ZEPHYR_SDK_VERSION}"

RUN west init --manifest-url "${ZEPHYR_MANIFEST_URL}" --manifest-rev "${ZEPHYR_MANIFEST_REV}" && \
  west update && west zephyr-export && \
  pip3 install --no-cache-dir -r "./zephyr/scripts/requirements.txt" && \
  chmod -R 775 "${ZEPHYR_WORKSPACE}"

ENV ZEPHYR_SDK_INSTALL_DIR=${ZEPHYR_SDK_INSTALL_DIR}/zephyr-sdk-${ZEPHYR_SDK_VERSION}
ENV ZEPHYR_BASE=${ZEPHYR_WORKSPACE}/zephyr

